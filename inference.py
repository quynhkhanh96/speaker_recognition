#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from hparam import hparam as hp
import numpy as np
import pandas as pd
import os, glob
import librosa
import random
from random import shuffle
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
from speech_embedder_net import SpeechEmbedder, GE2ELoss
from utils import get_centroids, get_cossim

TEST_PATH = '/home/khanhdtq/Downloads/vivos_test2'
TEST_PROCESSED_PATH = '/home/khanhdtq/processed_vietnamese/test_tisv'
test_audio_path = glob.glob(os.path.dirname(TEST_PATH + '/*/*.wav'))

MODEL_PATH = '/home/khanhdtq/PycharmProjects/speaker_verification/PyTorch_Speaker_Verification/ckpt_epoch_1320_batch_id_141.pth'
LOSS_PATH = '/home/khanhdtq/PycharmProjects/speaker_verification/PyTorch_Speaker_Verification/ckpt_loss_epoch_1320_batch_id_141.pth'
NUM_SPEAKER_PER_BATCH = 4
NUM_UTTERANCE_PER_SPEAKER = 5
thresh = 0.75

def save_spectrogram_tisv_test_time():
    """ Full preprocess of text independent utterance. The log-mel-spectrogram is saved as numpy file.
        Each partial utterance is splitted by voice detection using DB
        and the first and the last 180 frames from each partial utterance are saved.
        Need : utterance data set (VTCK)
    """
    print("start text independent utterance feature extraction")
    os.makedirs(hp.data.train_path, exist_ok=True)   # make folder to save train file
    os.makedirs(hp.data.test_path, exist_ok=True)    # make folder to save test file

    utter_min_len = (hp.data.tisv_frame * hp.data.hop + hp.data.window) * hp.data.sr    # lower bound of utterance length
    total_speaker_num = len(test_audio_path)

    print("total test speaker number : %d"%total_speaker_num)

    for i, folder in enumerate(test_audio_path):
        print("%dth speaker processing..."%i)
        utterances_spec = []
        for utter_name in os.listdir(folder):
            if utter_name[-4:].lower() == '.wav':
                utter_path = os.path.join(folder, utter_name)         # path of each utterance
                utter, sr = librosa.core.load(utter_path, hp.data.sr)        # load utterance audio
                intervals = librosa.effects.split(utter, top_db=30)         # voice activity detection
                for interval in intervals:
                    if (interval[1]-interval[0]) > utter_min_len:           # If partial utterance is sufficient long,
                        utter_part = utter[interval[0]:interval[1]]         # save first and last 180 frames of spectrogram.
                        S = librosa.core.stft(y=utter_part, n_fft=hp.data.nfft,
                                              win_length=int(hp.data.window * sr),
                                              hop_length=int(hp.data.hop * sr))
                        S = np.abs(S) ** 2
                        mel_basis = librosa.filters.mel(sr=hp.data.sr, n_fft=hp.data.nfft,
                                                        n_mels=hp.data.nmels)
                        S = np.log10(np.dot(mel_basis, S) + 1e-6)           # log mel spectrogram of utterances
                        utterances_spec.append(S[:, :hp.data.tisv_frame])    # first 180 frames of partial utterance
                        utterances_spec.append(S[:, -hp.data.tisv_frame:])   # last 180 frames of partial utterance

        utterances_spec = np.array(utterances_spec)
        print(utterances_spec.shape)
        # save spectrogram as numpy file
        np.save(os.path.join(TEST_PROCESSED_PATH, "speaker%d.npy"%i), utterances_spec)


class TestSpeakerDatasetTIMITPreprocessed(Dataset):

    def __init__(self, shuffle=True, utter_start=0):

        # data path
        self.path = TEST_PROCESSED_PATH
        self.utter_num = NUM_UTTERANCE_PER_SPEAKER
        self.file_list = os.listdir(self.path)
        self.shuffle = shuffle
        self.utter_start = utter_start

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):

        np_file_list = os.listdir(self.path)

        if self.shuffle:
            selected_file = random.sample(np_file_list, 1)[0]  # select random speaker
        else:
            selected_file = np_file_list[idx]

        utters = np.load(os.path.join(self.path, selected_file))  # load utterance spectrogram of selected speaker
        if self.shuffle:
            utter_index = np.random.randint(0, utters.shape[0], self.utter_num)  # select M utterances per speaker
            utterance = utters[utter_index]
        else:
            utterance = utters[
                        self.utter_start: self.utter_start + self.utter_num]  # utterances of a speaker [batch(M), n_mels, frames]

        utterance = utterance[:, :, :160]  # TODO implement variable length batch size

        utterance = torch.tensor(np.transpose(utterance, axes=(0, 2, 1)))  # transpose [batch, frames, n_mels]
        utterance = (utterance - utterance.mean()) / utterance.std()

        return utterance

if __name__ == '__main__':

    device = torch.device(hp.device)
    # Preprocessing
    # save_spectrogram_tisv_test_time()

    dataset_name = TEST_PATH.split('/')[-1]
    # Data loader
    test_dataset = TestSpeakerDatasetTIMITPreprocessed(shuffle=False)
    test_loader = DataLoader(test_dataset,
                             batch_size=NUM_SPEAKER_PER_BATCH,
                             shuffle=True,
                             # num_workers=hp.test.num_workers,
                             drop_last=True)

    # Embedding network
    embedder_net = SpeechEmbedder().to(device)
    embedder_net.load_state_dict(torch.load(MODEL_PATH))
    embedder_net.eval()
    # GE2E loss
    ge2e_loss = GE2ELoss(device)
    ge2e_loss.load_state_dict(torch.load(LOSS_PATH))

    # Inference
    os.makedirs('./infer_rs', exist_ok=True)
    os.makedirs(os.path.join('./infer_rs', dataset_name), exist_ok=True)

    batch_avg_EER = 0
    for batch_id, mel_db_batch in enumerate(test_loader):
        # print(mel_db_batch.shape)
        enrollment_batch = mel_db_batch
        verification_batch = mel_db_batch

        enrollment_batch = torch.reshape(enrollment_batch, (NUM_SPEAKER_PER_BATCH * NUM_UTTERANCE_PER_SPEAKER,
                                                            enrollment_batch.size(2),
                                                            enrollment_batch.size(3)))
        verification_batch = torch.reshape(verification_batch, (NUM_SPEAKER_PER_BATCH * NUM_UTTERANCE_PER_SPEAKER,
                                                                verification_batch.size(2),
                                                                verification_batch.size(3)))

        enrollment_embeddings = embedder_net(enrollment_batch.cuda())
        verification_embeddings = embedder_net(verification_batch.cuda())

        enrollment_embeddings = torch.reshape(enrollment_embeddings, (
            NUM_SPEAKER_PER_BATCH, NUM_UTTERANCE_PER_SPEAKER, enrollment_embeddings.size(1)
        ))

        verification_embeddings = torch.reshape(verification_embeddings, (
            NUM_SPEAKER_PER_BATCH, NUM_UTTERANCE_PER_SPEAKER, verification_embeddings.size(1)
        ))

        enrollment_centroids = get_centroids(enrollment_embeddings)

        sim_matrix = get_cossim(verification_embeddings, enrollment_centroids)

        # with torch.no_grad():
        #     ge2e_loss.eval()
        #     sim_matrix = ge2e_loss.inference(verification_embeddings, enrollment_centroids)
        sim_matrix_thresh = sim_matrix > thresh
        # calculating EER
        # diff = 1; EER = 0; EER_thresh = 0; EER_FAR = 0; EER_FRR = 0
        #
        # for thres in [0.01 * i + 0.5 for i in range(50)]:
        #     sim_matrix_thresh = sim_matrix > thres
        #
        #     FAR = (sum([sim_matrix_thresh[i].float().sum() - sim_matrix_thresh[i, :, i].float().sum() for i in
        #                 range(int(hp.test.N))])
        #            / (hp.test.N - 1.0) / (float(hp.test.M / 2)) / hp.test.N)
        #
        #     FRR = (sum([hp.test.M / 2 - sim_matrix_thresh[i, :, i].float().sum() for i in range(int(hp.test.N))])
        #            / (float(hp.test.M / 2)) / hp.test.N)
        #
        #     # Save threshold when FAR = FRR (=EER)
        #     if diff > abs(FAR - FRR):
        #         diff = abs(FAR - FRR)
        #         EER = (FAR + FRR) / 2
        #         EER_thresh = thres
        #         EER_FAR = FAR
        #         EER_FRR = FRR
        # batch_avg_EER += EER
        # print("\nEER : %0.2f (thres:%0.2f, FAR:%0.2f, FRR:%0.2f)" % (EER, EER_thresh, EER_FAR, EER_FRR))

        batch_simm_df = pd.DataFrame.from_records(np.reshape(sim_matrix.detach().numpy(), (-1, NUM_SPEAKER_PER_BATCH)))
        # batch_simm_df = pd.DataFrame.from_records(np.reshape(sim_matrix.cpu().numpy(), (-1, NUM_SPEAKER_PER_BATCH)))
        batch_simm_df.to_csv(os.path.join('./infer_rs', dataset_name, 'cossimm_{}_{}.csv'.format(dataset_name, batch_id)), index=False)

        sim_matrix_thresh = sim_matrix_thresh.cpu().numpy()

        batch_simm_thres_df = pd.DataFrame.from_records(np.reshape(sim_matrix_thresh, (-1, NUM_SPEAKER_PER_BATCH)))
        batch_simm_thres_df.to_csv(os.path.join('./infer_rs', dataset_name, 'simm_thres_{}_{}.csv'.format(dataset_name, batch_id)), index=False)

        # for file_ in os.listdir(TEST_PROCESSED_PATH):
        #     os.remove(os.path.join(TEST_PROCESSED_PATH, file_))




