import os
import numpy as np
import pandas as pd
import random
import time

import torch
import torch.nn as nn

from hparam import hparam as hp
from speech_embedder_net import SpeechEmbedder, GE2ELoss
from softmax_pretrain import SpeakerClassifier

from data_load import SpeakerDatasetTIMITPreprocessed
from torch.utils.data import DataLoader
from speech_embedder_net import SpeechEmbedder, GE2ELoss, get_centroids, get_cossim

FROZEN_EMBEDDER_NET = False
UNFREEZE_EPOCH = 10
CHECKPOINT_DIR = '/home/khanhdtq/ge2e_preprocess/finetune_checkpoint_epoch10q'
FINAL_MODEL = '/home/khanhdtq/ge2e_preprocess/finetune_checkpoint_epoch10q/ckpt_epoch_950_batch_id_141.pth'
# FINAL_MODEL = '/home/khanhdtq/PycharmProjects/speaker_verification/PyTorch_Speaker_Verification/speech_id_checkpoint/ckpt_epoch_990_batch_id_141.pth'
RESUME_EPOCH = 0


# Finetune 30 epochs
# ckpt 990:
# Vin Test: 0.0687 (N: 72, M: 16), 0.0866 (N: 72, M: 6)
# VIVOS Test: 0.0585 (N: 65, M: 16), 0.0698 (N: 65, 6)
# Finetune 101 epochs
# ckpt 950:
# Vin Test: 0.0620 (N: 72, M: 16), 0.0797 (N: 72, M: 6)
# VIVOS Test: 0.0570 (N: 65, M: 16), 0.070 (N: 65, M: 6)
# No finetune
# ckpt 990:
# Vin Test: 0.0594 (N: 72, M: 16), 0.0732 (N: 72, M: 6)
# VIVOS Test: 0.0434 (N: 65, M: 16), 0.0543 (N: 65, M: 6)


def frozen_whole_net(net):
    for child in net.children():
        print("child ", child, " was frozen")
        for param in child.parameters():
            param.requires_grad = False

    return net

def unfrozen_whole_net(net):
    for child in net.children():
        print("child ", child, " was unfrozen")
        for param in child.parameters():
            param.requires_grad = True

    return net

def finetune():

    # data loader
    train_dataset = SpeakerDatasetTIMITPreprocessed('train')
    train_loader = DataLoader(train_dataset, batch_size=hp.train.N,
                              shuffle=True, num_workers=hp.train.num_workers,
                              drop_last=True)

    valid_dataset = SpeakerDatasetTIMITPreprocessed('valid')
    valid_loader = DataLoader(valid_dataset, batch_size=hp.test.N,
                              shuffle=True, num_workers=hp.test.num_workers,
                              drop_last=True)

    # speaker embedding network
    device = torch.device(hp.device)
    classifier = SpeakerClassifier()
    checkpoint = torch.load('/home/khanhdtq/PycharmProjects/speaker_verification/sotmax_pretrain_checkpoints/cls_101_3.8434')
    classifier.load_state_dict(checkpoint)

    speaker_embedding_model = classifier.embber_net.cuda()
    print('sucessfully loaded!')

    speaker_embedding_model = SpeechEmbedder().to(device)
    # GE2E loss
    ge2e_loss = GE2ELoss(device)

    if hp.train.restore:
        speaker_embedding_model.load_state_dict(torch.load(hp.model.model_path))
        ge2e_loss.load_state_dict(torch.load(hp.model.loss_path))

    # optimizer
    optimizer = torch.optim.SGD([
        {'params': speaker_embedding_model.parameters()},
        {'params': ge2e_loss.parameters()}
        ], lr=hp.train.lr)

    def adjust_learning_rate(optimizer, epoch):
        if epoch >= 300 and (epoch + 1) % 50 == 0:
            # lr = hp.train.lr * 0.8
            lr = 0.01
            for param_group in optimizer.param_groups:
                param_group['lr'] *= 0.8
                lr = param_group['lr']
            print('Changed learning rate to {}'.format(lr))

    if FROZEN_EMBEDDER_NET:
        speaker_embedding_model = frozen_whole_net(speaker_embedding_model)

    os.makedirs(CHECKPOINT_DIR, exist_ok=True)
    iteration = 0
    best_loss = 10000.
    for e in range(RESUME_EPOCH, hp.train.epochs):
    # for e in range(370):

        if FROZEN_EMBEDDER_NET and (e + 1) == UNFREEZE_EPOCH:
            speaker_embedding_model = unfrozen_whole_net(speaker_embedding_model)

        total_loss = 0.
        speaker_embedding_model.train()
        ge2e_loss.train()

        adjust_learning_rate(optimizer, e)

        for batch_id, mel_db_batch in enumerate(train_loader):
            mel_db_batch = mel_db_batch.cuda()
            # print('mel_db_batch before reshaping: {}'.format(mel_db_batch.shape))
            mel_db_batch = torch.reshape(mel_db_batch,
                                         (hp.train.N * hp.train.M, mel_db_batch.size(2), mel_db_batch.size(3)))
            perm = random.sample(range(0, hp.train.N * hp.train.M), hp.train.N * hp.train.M)
            unperm = list(perm)
            for i, j in enumerate(perm):
                unperm[j] = i
            mel_db_batch = mel_db_batch[perm]
            # gradient accumulates
            optimizer.zero_grad()

            embeddings = speaker_embedding_model(mel_db_batch)
            embeddings = embeddings[unperm]
            embeddings = torch.reshape(embeddings, (hp.train.N, hp.train.M, embeddings.size(1)))

            # get loss, call backward, step optimizer
            loss = ge2e_loss(embeddings)  # wants (Speaker, Utterances, embedding)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(speaker_embedding_model.parameters(), 3.0)
            torch.nn.utils.clip_grad_norm_(ge2e_loss.parameters(), 1.0)
            optimizer.step()

            total_loss = total_loss + loss  # .item() #/ (hp.train.N * hp.train.M)
            iteration += 1
            if (batch_id + 1) % hp.train.log_interval == 0:
                mesg = "{0}\tEpoch:{1}[{2}/{3}], Iteration:{4}\tLoss:{5:.4f}\tTLoss:{6:.4f}".format(time.ctime(),
                                                                                                    e + 1 + RESUME_EPOCH,
                                                                                                    batch_id + 1, len(
                        train_dataset) // hp.train.N, iteration, loss, total_loss / (batch_id + 1))
                print(mesg)
                if hp.train.log_file is not None:
                    with open(hp.train.log_file, 'a') as f:
                        f.write(mesg + '\n')

        # validate model
        speaker_embedding_model.eval()
        val_total_loss = 0
        count = 0
        for val_batch_id, val_mel_db_batch in enumerate(valid_loader):
            val_mel_db_batch = val_mel_db_batch.cuda()

            val_mel_db_batch = torch.reshape(val_mel_db_batch, (
                hp.test.N * hp.test.M, val_mel_db_batch.size(2), val_mel_db_batch.size(3)))
            val_perm = random.sample(range(0, hp.test.N * hp.test.M), hp.test.N * hp.test.M)
            val_unperm = list(val_perm)
            for i, j in enumerate(val_perm):
                val_unperm[j] = i
            val_mel_db_batch = val_mel_db_batch[val_perm]
            val_embeddings = speaker_embedding_model(val_mel_db_batch)
            val_embeddings = val_embeddings[val_unperm]
            val_embeddings = torch.reshape(val_embeddings, (hp.test.N, hp.test.M, val_embeddings.size(1)))

            with torch.no_grad():
                ge2e_loss.eval()
                val_loss = ge2e_loss(val_embeddings)
            val_total_loss = val_total_loss + val_loss.item()  # / (hp.test.N * hp.test.M)
            count += 1

        val_mesg = "\t\t--- Val Tloss:{0:.4f}\n".format(val_total_loss / count)
        if hp.train.log_file is not None:
            with open(hp.train.log_file, 'a') as f:
                f.write(val_mesg)
        print(val_mesg)

        # if hp.train.checkpoint_dir is not None and (e + 1 + RESUME_EPOCH) % hp.train.checkpoint_interval == 0:
        if val_total_loss < best_loss and e >= 200:
            # save embedder net
            # speaker_embedding_model.eval() #.cpu()
            ckpt_model_filename = "ckpt_epoch_" + str(e + 1 + RESUME_EPOCH) + "_batch_id_" + str(batch_id + 1) + ".pth"
            ckpt_model_path = os.path.join(CHECKPOINT_DIR, ckpt_model_filename)
            torch.save(speaker_embedding_model.state_dict(), ckpt_model_path)
            speaker_embedding_model.train()
            # save ge2e loss
            # ge2e_loss.eval().cpu()
            ckpt_loss_filename = "ckpt_loss_epoch_" + str(e + 1 + RESUME_EPOCH) + "_batch_id_" + str(batch_id + 1) + ".pth"
            ckpt_loss_path = os.path.join(CHECKPOINT_DIR, ckpt_loss_filename)
            torch.save(ge2e_loss.state_dict(), ckpt_loss_path)

    # save model
    speaker_embedding_model.eval().cpu()
    save_model_filename = "final_epoch_" + str(e + 1 + RESUME_EPOCH) + "_batch_id_" + str(batch_id + 1) + ".model"
    save_model_path = os.path.join(CHECKPOINT_DIR, save_model_filename)
    torch.save(speaker_embedding_model.state_dict(), save_model_path)

    print("\nDone, trained model saved at", save_model_path)

def evaluate():

    test_dataset = SpeakerDatasetTIMITPreprocessed('test')
    test_loader = DataLoader(test_dataset, batch_size=hp.test.N,
                              shuffle=True, num_workers=hp.test.num_workers,
                              drop_last=True)

    device = torch.device(hp.device)
    embedder_net = SpeechEmbedder().to(device)
    embedder_net.load_state_dict(torch.load(FINAL_MODEL))
    embedder_net.eval()

    avg_EER = 0
    for e in range(hp.test.epochs):
        batch_avg_EER = 0
        for batch_id, mel_db_batch in enumerate(test_loader):
            assert hp.test.M % 2 == 0
            mel_db_batch = mel_db_batch.cuda()
            enrollment_batch, verification_batch = torch.split(mel_db_batch, int(mel_db_batch.size(1) / 2), dim=1)

            enrollment_batch = torch.reshape(enrollment_batch, (hp.test.N * hp.test.M // 2,
                                                                enrollment_batch.size(2),
                                                                enrollment_batch.size(3)))
            verification_batch = torch.reshape(verification_batch, (hp.test.N * hp.test.M // 2,
                                                                    verification_batch.size(2),
                                                                    verification_batch.size(3)))

            perm = random.sample(range(0, verification_batch.size(0)), verification_batch.size(0))
            unperm = list(perm)
            for i, j in enumerate(perm):
                unperm[j] = i

            verification_batch = verification_batch[perm]
            enrollment_embeddings = embedder_net(enrollment_batch)
            verification_embeddings = embedder_net(verification_batch)
            verification_embeddings = verification_embeddings[unperm]

            enrollment_embeddings = torch.reshape(enrollment_embeddings,
                                                  (hp.test.N, hp.test.M // 2, enrollment_embeddings.size(1)))
            verification_embeddings = torch.reshape(verification_embeddings,
                                                    (hp.test.N, hp.test.M // 2, verification_embeddings.size(1)))

            enrollment_centroids = get_centroids(enrollment_embeddings)

            sim_matrix = get_cossim(verification_embeddings, enrollment_centroids)

            # calculating EER
            diff = 1; EER = 0; EER_thresh = 0; EER_FAR = 0; EER_FRR = 0

            for thres in [0.01 * i + 0.5 for i in range(50)]:
                sim_matrix_thresh = sim_matrix > thres

                FAR = (sum([sim_matrix_thresh[i].float().sum() - sim_matrix_thresh[i, :, i].float().sum() for i in
                            range(int(hp.test.N))])
                       / (hp.test.N - 1.0) / (float(hp.test.M / 2)) / hp.test.N)

                FRR = (sum([hp.test.M / 2 - sim_matrix_thresh[i, :, i].float().sum() for i in range(int(hp.test.N))])
                       / (float(hp.test.M / 2)) / hp.test.N)

                # Save threshold when FAR = FRR (=EER)
                if diff > abs(FAR - FRR):
                    diff = abs(FAR - FRR)
                    EER = (FAR + FRR) / 2
                    EER_thresh = thres
                    EER_FAR = FAR
                    EER_FRR = FRR
            batch_avg_EER += EER
            print("\nEER : %0.2f (thres:%0.2f, FAR:%0.2f, FRR:%0.2f)" % (EER, EER_thresh, EER_FAR, EER_FRR))
        avg_EER += batch_avg_EER / (batch_id + 1)
    avg_EER = avg_EER / hp.test.epochs
    print("\n EER across {0} epochs: {1:.4f}".format(hp.test.epochs, avg_EER))


if __name__ == '__main__':

    # finetune()

    evaluate()


