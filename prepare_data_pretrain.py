import os
import glob
import numpy as np
import pandas as pd

DATA_PATH = '/home/khanhdtq/Downloads/vietnamese_wav'

list_speakers = os.listdir(DATA_PATH)

class_dict = {}
trn_spkrs = []; val_spkrs = []
i = 0
for spkr in list_speakers:
    spkr_files = os.listdir(os.path.join(DATA_PATH, spkr))
    if len(spkr_files) > 9:
        trn_spkr_files = spkr_files[: int(0.8 * len(spkr_files))]
        val_spkr_files = spkr_files[int(0.8 * len(spkr_files)): ]
        for file_ in trn_spkr_files:
            trn_spkrs.append(os.path.join(DATA_PATH, spkr, file_))
        for file_ in val_spkr_files:
            val_spkrs.append(os.path.join(DATA_PATH, spkr, file_))

        class_dict[spkr] = i
        i += 1

os.makedirs('../meta', exist_ok=True)
with open('../meta/vin_train.txt', 'a') as f:
    for path_ in trn_spkrs:
        label = class_dict[path_.split('/')[-2]]
        f.write('{} {}\n'.format(path_, label))

with open('../meta/vin_val.txt', 'a') as f:
    for path_ in val_spkrs:
        label = class_dict[path_.split('/')[-2]]
        f.write('{} {}\n'.format(path_, label))

print('Num speaker: {}'.format(len(class_dict.keys())))
df = pd.DataFrame(list(class_dict.items()), columns=['speaker', 'label'])
df.to_csv('../meta/label.csv', index=False)