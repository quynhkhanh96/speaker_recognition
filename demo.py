from hparam import hparam as hp
import numpy as np
import pandas as pd
import os, glob
import shutil
import librosa
import random
from random import shuffle
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
from speech_embedder_net import SpeechEmbedder, GE2ELoss
from utils import get_centroids, get_cossim

WAV_PATH = '/home/khanhdtq/demo_ge2e/wav'
ENROLLED_PATH = '/home/khanhdtq/demo_ge2e/enrolled'
MODEL_PATH = '/home/khanhdtq/PycharmProjects/speaker_verification/PyTorch_Speaker_Verification/speech_id_checkpoint/ckpt_epoch_1320_batch_id_141.pth'
TMP_PATH = '/home/khanhdtq/demo_ge2e/temp'
RESULT_PATH = '/home/khanhdtq/demo_ge2e/result'
PROCESSED_LOG = os.path.join(WAV_PATH, 'processed_logs.txt')
NUM_SPEAKER_PER_BATCH = 1
NUM_UTTERANCE_PER_SPEAKER = 2
NUM_CENTROIDS_PER_BATCH = 4

def clean_folder(folder_path):
    for sub_folder in os.listdir(folder_path):
        shutil.rmtree(os.path.join(folder_path, sub_folder), ignore_errors=True)


def save_spectrogram_tisv_test_time():

    test_audio_path = glob.glob(WAV_PATH + '/*/*.wav')
    utter_min_len = (hp.data.tisv_frame * hp.data.hop + hp.data.window) * hp.data.sr    # lower bound of utterance length

    try:
        with open(PROCESSED_LOG, 'r') as log:
            content = log.readlines()
        processed_files = [x.strip() for x in content]
        test_audio_path = list(set(test_audio_path) - set(processed_files))
    except:
        pass

    test_folders = list(set([path_.split('/')[-2] for path_ in test_audio_path]))
    for i, folder in enumerate(test_folders):
        print("%dth speaker processing..." % i)

        new_utterances = [utter for utter in test_audio_path if utter.split('/')[-2] == folder]
        utterances_spec = []

        for utter_name in new_utterances:
            utter, sr = librosa.core.load(utter_name, hp.data.sr)  # load utterance audio
            intervals = librosa.effects.split(utter, top_db=30)  # voice activity detection
            for interval in intervals:
                if (interval[1] - interval[0]) > utter_min_len:  # If partial utterance is sufficient long,
                    utter_part = utter[interval[0]:interval[1]]  # save first and last 180 frames of spectrogram.
                    S = librosa.core.stft(y=utter_part, n_fft=hp.data.nfft,
                                          win_length=int(hp.data.window * sr),
                                          hop_length=int(hp.data.hop * sr))
                    S = np.abs(S) ** 2
                    mel_basis = librosa.filters.mel(sr=hp.data.sr, n_fft=hp.data.nfft,
                                                    n_mels=hp.data.nmels)
                    S = np.log10(np.dot(mel_basis, S) + 1e-6)  # log mel spectrogram of utterances
                    utterances_spec.append(S[:, :hp.data.tisv_frame])  # first 180 frames of partial utterance
                    utterances_spec.append(S[:, -hp.data.tisv_frame:])  # last 180 frames of partial utterance

        utterances_spec = np.array(utterances_spec)
        print(utterances_spec.shape)
        # save spectrogram as numpy file
        np.save(os.path.join(TMP_PATH, "{}.npy".format(folder)), utterances_spec)

class TestSpeakerDatasetPreprocessed(Dataset):

    def __init__(self, shuffle=True, utter_start=0):

        # data path
        self.path = TMP_PATH
        self.utter_num = NUM_UTTERANCE_PER_SPEAKER
        self.file_list = os.listdir(self.path)
        self.shuffle = shuffle
        self.utter_start = utter_start

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):

        np_file_list = os.listdir(self.path)

        if self.shuffle:
            selected_file = random.sample(np_file_list, 1)[0]  # select random speaker
        else:
            selected_file = np_file_list[idx]

        utters = np.load(os.path.join(self.path, selected_file))  # load utterance spectrogram of selected speaker
        if self.shuffle:
            utter_index = np.random.randint(0, utters.shape[0], self.utter_num)  # select M utterances per speaker
            utterance = utters[utter_index]
        else:
            utterance = utters[
                        self.utter_start: self.utter_start + self.utter_num]  # utterances of a speaker [batch(M), n_mels, frames]

        utterance = utterance[:, :, :160]  # TODO implement variable length batch size

        utterance = torch.tensor(np.transpose(utterance, axes=(0, 2, 1)))  # transpose [batch, frames, n_mels]
        utterance = (utterance - utterance.mean()) / utterance.std()

        return selected_file.split('.')[0], utterance # return speaker id and melspectrogram

class EmbeddingsGetter(Dataset):

    def __init__(self, load_embedding=True):

        self.path = ENROLLED_PATH
        self.file_list = os.listdir(self.path)
        self.load_embedding = load_embedding

    def __getitem__(self, idx):
        if self.load_embedding:
            embeddings = torch.load(os.path.join(self.path, 'emb_chunk_{}.pt'.format(idx)))
            return embeddings
        centroids = torch.load(os.path.join(self.path, 'cent_chunk_{}.pt'.format(idx)))
        return centroids


if __name__ == '__main__':

    device = torch.device(hp.device)

    save_spectrogram_tisv_test_time()

    test_dataset = TestSpeakerDatasetPreprocessed(shuffle=False)
    test_loader = DataLoader(test_dataset,
                             batch_size=NUM_SPEAKER_PER_BATCH,
                             shuffle=True,
                             # num_workers=hp.test.num_workers,
                             drop_last=True)

    # Embedding network
    embedder_net = SpeechEmbedder() #.to(device)
    embedder_net.load_state_dict(torch.load(MODEL_PATH))
    embedder_net.eval()

    if len(os.listdir(ENROLLED_PATH)) == 0:
        for spkr_id_batch, enrollment_batch in test_loader:
            enrollment_batch = torch.reshape(enrollment_batch, (NUM_SPEAKER_PER_BATCH * NUM_UTTERANCE_PER_SPEAKER,
                                                                enrollment_batch.size(2),
                                                                enrollment_batch.size(3)))

            enrollment_embeddings = embedder_net(enrollment_batch)

            enrollment_embeddings = torch.reshape(enrollment_embeddings, (
                NUM_SPEAKER_PER_BATCH, NUM_UTTERANCE_PER_SPEAKER, enrollment_embeddings.size(1)
            ))

            enrollment_centroids = get_centroids(enrollment_embeddings)

            enrollment_centroids = enrollment_centroids.detach().numpy()

            for i, spkr_id in enumerate(spkr_id_batch):
                np.save(os.path.join(ENROLLED_PATH, '{}.npy'.format(spkr_id)), enrollment_centroids[i])

    else:

        cent_getter = EmbeddingsGetter(False)
        cent_loader = DataLoader(cent_getter, batch_size=NUM_SPEAKER_PER_BATCH)

        for spkr_id_batch, mel_db_batch in test_loader:
            mel_db_batch = torch.reshape(mel_db_batch, (NUM_SPEAKER_PER_BATCH * NUM_UTTERANCE_PER_SPEAKER,
                                                                mel_db_batch.size(2),
                                                                mel_db_batch.size(3)))

            batch_embeddings = embedder_net(mel_db_batch)

            batch_embeddings = torch.reshape(batch_embeddings, (
                NUM_SPEAKER_PER_BATCH, NUM_UTTERANCE_PER_SPEAKER, batch_embeddings.size(1)
            ))




    clean_folder(WAV_PATH)
    for file_ in os.listdir(TMP_PATH):
        os.remove(os.path.join(TMP_PATH, file_))








