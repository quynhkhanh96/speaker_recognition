#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from hparam import hparam as hp
import numpy as np
import pandas as pd
import os, glob
import librosa
import random
from random import shuffle
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
from speech_embedder_net import SpeechEmbedder, GE2ELoss
from utils import get_centroids, get_cossim

TEST_PATH = '/home/khanhdtq/Downloads/test_ge2e/vivos_test0'
TEST_PROCESSED_PATH = '/home/khanhdtq/Downloads/test_ge2e/preprocessed'
test_audio_path = glob.glob(os.path.dirname(TEST_PATH + '/*/*.wav'))

MODEL_PATH = '/home/khanhdtq/PycharmProjects/speaker_verification/PyTorch_Speaker_Verification/model.model'
LOSS_PATH = ''
NUM_SPEAKER_PER_BATCH = 7
NUM_UTTERANCE_PER_SPEAKER = 5
thresh = 0.75

EMBEDDING_PATH = ''
NUM_CENTROIDS_PER_BATCH = 10
RESULT_PATH = ''

def save_spectrogram_tisv_test_time():
    """ Full preprocess of text independent utterance. The log-mel-spectrogram is saved as numpy file.
        Each partial utterance is splitted by voice detection using DB
        and the first and the last 180 frames from each partial utterance are saved.
        Need : utterance data set (VTCK)
    """
    print("start text independent utterance feature extraction")
    os.makedirs(hp.data.train_path, exist_ok=True)   # make folder to save train file
    os.makedirs(hp.data.test_path, exist_ok=True)    # make folder to save test file

    utter_min_len = (hp.data.tisv_frame * hp.data.hop + hp.data.window) * hp.data.sr    # lower bound of utterance length
    total_speaker_num = len(test_audio_path)

    print("total test speaker number : %d"%total_speaker_num)

    for i, folder in enumerate(test_audio_path):
        print("%dth speaker processing..."%i)
        utterances_spec = []
        for utter_name in os.listdir(folder):
            if utter_name[-4:].lower() == '.wav':
                utter_path = os.path.join(folder, utter_name)         # path of each utterance
                utter, sr = librosa.core.load(utter_path, hp.data.sr)        # load utterance audio
                intervals = librosa.effects.split(utter, top_db=30)         # voice activity detection
                for interval in intervals:
                    if (interval[1]-interval[0]) > utter_min_len:           # If partial utterance is sufficient long,
                        utter_part = utter[interval[0]:interval[1]]         # save first and last 180 frames of spectrogram.
                        S = librosa.core.stft(y=utter_part, n_fft=hp.data.nfft,
                                              win_length=int(hp.data.window * sr),
                                              hop_length=int(hp.data.hop * sr))
                        S = np.abs(S) ** 2
                        mel_basis = librosa.filters.mel(sr=hp.data.sr, n_fft=hp.data.nfft,
                                                        n_mels=hp.data.nmels)
                        S = np.log10(np.dot(mel_basis, S) + 1e-6)           # log mel spectrogram of utterances
                        utterances_spec.append(S[:, :hp.data.tisv_frame])    # first 180 frames of partial utterance
                        utterances_spec.append(S[:, -hp.data.tisv_frame:])   # last 180 frames of partial utterance

        utterances_spec = np.array(utterances_spec)
        print(utterances_spec.shape)
        # save spectrogram as numpy file
        np.save(os.path.join(TEST_PROCESSED_PATH, "speaker%d.npy"%i), utterances_spec)


class TestSpeakerDatasetTIMITPreprocessed(Dataset):

    def __init__(self, shuffle=True, utter_start=0):

        # data path
        self.path = TEST_PROCESSED_PATH
        self.utter_num = NUM_UTTERANCE_PER_SPEAKER

        self.file_list = os.listdir(self.path)
        self.shuffle = shuffle
        self.utter_start = utter_start

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):

        np_file_list = os.listdir(self.path)

        if self.shuffle:
            selected_file = random.sample(np_file_list, 1)[0]  # select random speaker
        else:
            selected_file = np_file_list[idx]

        utters = np.load(os.path.join(self.path, selected_file))  # load utterance spectrogram of selected speaker
        if self.shuffle:
            utter_index = np.random.randint(0, utters.shape[0], self.utter_num)  # select M utterances per speaker
            utterance = utters[utter_index]
        else:
            utterance = utters[
                        self.utter_start: self.utter_start + self.utter_num]  # utterances of a speaker [batch(M), n_mels, frames]

        utterance = utterance[:, :, :160]  # TODO implement variable length batch size

        utterance = torch.tensor(np.transpose(utterance, axes=(0, 2, 1)))  # transpose [batch, frames, n_mels]
        return utterance

class EmbeddingsGetter(Dataset):

    def __init__(self, load_embedding=True):

        self.path = EMBEDDING_PATH
        self.file_list = os.listdir(self.path)
        self.load_embedding = load_embedding

    def __getitem__(self, idx):
        if self.load_embedding:
            embeddings = torch.load(os.path.join(self.path, 'emb_chunk_{}.pt'.format(idx)))
            return embeddings
        centroids = torch.load(os.path.join(self.path, 'cent_chunk_{}.pt'.format(idx)))
        return centroids

if __name__ == '__main__':
    # Preprocessing
    save_spectrogram_tisv_test_time()

    dataset_name = TEST_PATH.split('/')[-1]
    # Data loader
    test_dataset = TestSpeakerDatasetTIMITPreprocessed(shuffle=False)
    test_loader = DataLoader(test_dataset,
                             batch_size=NUM_SPEAKER_PER_BATCH,
                             shuffle=True,
                             num_workers=hp.test.num_workers,
                             drop_last=True)

    # Embedding network
    embedder_net = SpeechEmbedder()
    embedder_net.load_state_dict(torch.load(MODEL_PATH))
    embedder_net.eval()
    # GE2E loss
    ge2e_loss = GE2ELoss()
    ge2e_loss.load_state_dict(torch.load(LOSS_PATH))

    # Inference
    os.makedirs('./infer_rs', exist_ok=True)
    os.makedirs(os.path.join('./infer_rs', dataset_name), exist_ok=True)

    # generate and save embeddings and centroids
    os.makedirs(EMBEDDING_PATH, exist_ok=True)
    for batch_id, mel_db_batch in enumerate(test_loader):
        # enrollment_batch = mel_db_batch
        verification_batch = mel_db_batch

        # enrollment_batch = torch.reshape(enrollment_batch, (NUM_SPEAKER_PER_BATCH * NUM_UTTERANCE_PER_SPEAKER,
        #                                                     enrollment_batch.size(2),
        #                                                     enrollment_batch.size(3)))
        verification_batch = torch.reshape(verification_batch, (NUM_SPEAKER_PER_BATCH * NUM_UTTERANCE_PER_SPEAKER,
                                                                verification_batch.size(2),
                                                                verification_batch.size(3)))

        # enrollment_embeddings = embedder_net(enrollment_batch)
        verification_embeddings = embedder_net(verification_batch)

        verification_embeddings = torch.reshape(verification_embeddings, (
            NUM_SPEAKER_PER_BATCH, NUM_UTTERANCE_PER_SPEAKER, verification_embeddings.size(1)
        ))

        enrollment_centroids = get_centroids(verification_embeddings)

        torch.save(verification_embeddings, os.path.join(EMBEDDING_PATH, 'emb_chunk_{}.pt'.format(batch_id)))
        torch.save(enrollment_centroids, os.path.join(EMBEDDING_PATH, 'cent_chunk_{}.pt'.format(batch_id)))

    # Calculate cosine similarity matrix
    emb_getter = EmbeddingsGetter()
    emb_loader = DataLoader(emb_getter, batch_size=NUM_CENTROIDS_PER_BATCH, drop_last=True)
    cent_getter = EmbeddingsGetter(False)
    cent_loader = DataLoader(cent_getter, batch_size=NUM_CENTROIDS_PER_BATCH)

    for i, batch_emb in enumerate(emb_loader):
        batch_list_df = []
        for _, batch_cent in enumerate(cent_loader):
            with torch.no_grad():
                ge2e_loss.eval()
                tmp_sim_matrix = ge2e_loss.inference(batch_emb, batch_cent)

            tmp_sim_matrix = tmp_sim_matrix.detach().numpy()
            tmp_sim_matrix = np.reshape(tmp_sim_matrix, (-1, NUM_CENTROIDS_PER_BATCH))
            tmp_df = pd.DataFrame.from_records(tmp_sim_matrix)
            batch_list_df.append(tmp_df)

        batch_df = pd.concat(batch_list_df, axis=1)
        batch_df.to_csv(os.path.join(RESULT_PATH, '{}.csv'.format(i)))



















