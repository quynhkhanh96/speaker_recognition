import os
import numpy as np
import pandas as pd
import random
import librosa
import glob
import time

import torch
import torch.nn as nn
import torch.nn.functional as F

from hparam import hparam as hp
from speech_embedder_net import SpeechEmbedder

from torch.utils.data import Dataset, DataLoader
from torch.optim import Adam
from torch.optim.lr_scheduler import CosineAnnealingLR
from fastprogress import master_bar, progress_bar
from tqdm import tqdm

PREPROCESSED_PATH = '/home/khanhdtq/ge2e_preprocess'
os.makedirs(PREPROCESSED_PATH, exist_ok=True)
AUDIO_PATH = '/home/khanhdtq/Downloads/vietnamese_wav'
DF_LABEL_PATH = '/home/khanhdtq/PycharmProjects/speaker_verification/meta/label.csv'
# NUM_SPEAKER_PER_BATCH = 32
BATCH_SIZE = 32
NUM_UTTER_PER_SPEAKER = 5 #5

N_CLASSES = 100

LEARNING_RATE = 0.01
N_EPOCHS = 100
RESUME_EPOCH = 30
RESUME_MODEL = '/home/khanhdtq/PycharmProjects/speaker_verification/sotmax_pretrain_checkpoints/cls_30_4.1006'

lr = 3e-3
eta_min = 1e-5
t_max = 10
# audio preprocessing ##############################################
def audio_preprocess(txt_file, mode):
    print('{} data processing ...'.format(mode.upper()))
    os.makedirs(os.path.join(PREPROCESSED_PATH, mode), exist_ok=True)
    utter_min_len = (hp.data.tisv_frame * hp.data.hop + hp.data.window) * hp.data.sr

    with open(txt_file) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    mode_utterances = [string.split()[0].split('/')[-1] for string in content]

    for i, folder in enumerate(os.listdir(AUDIO_PATH)):
        print("%dth speaker processing..." % i)
        utterances_spec = []
        for utter_name in os.listdir(os.path.join(AUDIO_PATH, folder)):
            if utter_name in mode_utterances:
                # if utter_name[-4:].lower() == '.wav':
                utter_path = os.path.join(AUDIO_PATH, folder, utter_name)  # path of each utterance
                utter, sr = librosa.core.load(utter_path, hp.data.sr)  # load utterance audio
                intervals = librosa.effects.split(utter, top_db=30)  # voice activity detection
                for interval in intervals:
                    if (interval[1] - interval[0]) > utter_min_len:  # If partial utterance is sufficient long,
                        utter_part = utter[
                                     interval[0]:interval[1]]  # save first and last 180 frames of spectrogram.
                        S = librosa.core.stft(y=utter_part, n_fft=hp.data.nfft,
                                              win_length=int(hp.data.window * sr), hop_length=int(hp.data.hop * sr))
                        S = np.abs(S) ** 2
                        mel_basis = librosa.filters.mel(sr=hp.data.sr, n_fft=hp.data.nfft, n_mels=hp.data.nmels)
                        S = np.log10(np.dot(mel_basis, S) + 1e-6)  # log mel spectrogram of utterances
                        utterances_spec.append(S[:, :hp.data.tisv_frame])  # first 180 frames of partial utterance
                        utterances_spec.append(S[:, -hp.data.tisv_frame:])  # last 180 frames of partial utterance

        utterances_spec = np.array(utterances_spec)

        for j in range(0, len(utterances_spec), NUM_UTTER_PER_SPEAKER):
            if j + NUM_UTTER_PER_SPEAKER < len(utterances_spec):
                utter_spec_chunk = utterances_spec[j: j + NUM_UTTER_PER_SPEAKER]
                np.save(os.path.join(PREPROCESSED_PATH, mode, '{}-{}.npy'.format(folder, j)), utter_spec_chunk)

        # np.save(os.path.join(PREPROCESSED_PATH, mode, "{}.npy".format(folder)), utterances_spec)

# dataset ###########################################################
class Vin300hSpeakerDataset(Dataset):

    def __init__(self, mode, df_label, shuffle=True, utter_start=0):

        if mode == 'train':
            self.path = os.path.join(PREPROCESSED_PATH, 'train')
            self.utter_num = NUM_UTTER_PER_SPEAKER
        elif mode == 'valid':
            self.path = os.path.join(PREPROCESSED_PATH, 'valid')
            self.utter_num = NUM_UTTER_PER_SPEAKER

        self.file_list = os.listdir(self.path)
        self.df_label = df_label
        self.shuffle = shuffle
        self.utter_start = utter_start

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):

        # Get speaker's melspectrogram of utterances
        np_file_list = os.listdir(self.path)

        if self.shuffle:
            selected_file = random.sample(np_file_list, 1)[0]  # select random speaker
        else:
            selected_file = np_file_list[idx]

        utters = np.load(os.path.join(self.path, selected_file))  # load utterance spectrogram of selected speaker
        if self.shuffle:
            utter_index = np.random.randint(0, utters.shape[0], self.utter_num)  # select M utterances per speaker
            utterance = utters[utter_index]
        else:
            utterance = utters[
                        self.utter_start: self.utter_start + self.utter_num]  # utterances of a speaker [M, n_mels, frames]

        utterance = utterance[:, :, :160]  # TODO implement variable length batch size

        utterance = torch.tensor(np.transpose(utterance, axes=(0, 2, 1)))  # transpose [M, frames, n_mels]
        utterance = (utterance - utterance.mean())/utterance.std()

        # Get speaker label
        speaker_label = self.df_label.loc[self.df_label['speaker'] == selected_file.split('-')[0]]['label'].values[0]
        y = np.zeros(N_CLASSES)
        y[speaker_label] = 1

        return utterance, torch.from_numpy(y).float()

# net ############################################################
class SpeakerClassifier(nn.Module):

    def __init__(self, hidden_size=512, n_classes=N_CLASSES):
        super(SpeakerClassifier, self).__init__()
        self.embber_net = SpeechEmbedder()
        self.l1 = nn.Linear(hp.model.proj, hidden_size)
        self.l2 = nn.Linear(hidden_size, n_classes)
        self.bn1 = nn.BatchNorm1d(hidden_size)
        self.bn2 = nn.BatchNorm1d(n_classes)

    def forward(self, x):
        x = self.embber_net(x)
        # print('out embedder_net shape: {}'.format(x.shape))
        # x = torch.reshape(x, (BATCH_SIZE, NUM_UTTER_PER_SPEAKER, hp.model.proj)).sum(dim=1)
        x = torch.mean(torch.reshape(x, (BATCH_SIZE, NUM_UTTER_PER_SPEAKER, hp.model.proj)), dim=1)
        # print('after reshape x shape: {}'.format(x.shape))
        x = self.l1(x)
        x = self.bn1(x)
        x = F.relu(x)

        x = self.l2(x)
        x = self.bn2(x)

        out = F.softmax(x, dim=1)

        return out

def adjust_learning_rate(optimizer, epoch):
    if (epoch + 1) % 20 == 0:
        for param_group in optimizer.param_groups:
            param_group['lr'] *= 0.8
            lr = param_group['lr']
        print('Changed learning rate to {}'.format(lr))

# trainer ######################################################
def train():

    df_label = pd.read_csv(DF_LABEL_PATH)
    # dataloader
    train_dataset = Vin300hSpeakerDataset(mode='train', df_label=df_label)
    train_loader = DataLoader(train_dataset, batch_size=BATCH_SIZE,
                              shuffle=True, num_workers=hp.train.num_workers,
                              drop_last=True)

    valid_dataset = Vin300hSpeakerDataset(mode='valid', df_label=df_label)
    valid_loader = DataLoader(valid_dataset, batch_size=BATCH_SIZE,
                              shuffle=True, num_workers=hp.test.num_workers,
                              drop_last=True)
    # with open('../meta/vin_val.txt', 'r') as f:
    #     content = f.readlines()
    # content = [x.strip() for x in content]
    # len_val = len(content)
    # print('Number of valid samples: {}'.format(len_val))

    device = torch.device(hp.device)
    # net
    classifier = SpeakerClassifier().to(device)
    if RESUME_EPOCH:
        classifier.load_state_dict(torch.load(RESUME_MODEL))

    # loss
    criterion = nn.CrossEntropyLoss().to(device)

    # optimizer
    optimizer = torch.optim.SGD(params=classifier.parameters(), lr=LEARNING_RATE,
                                momentum=0.99)
    # optimizer = Adam(params=classifier.parameters(), lr=lr, amsgrad=False)
    # scheduler = CosineAnnealingLR(optimizer, T_max=t_max, eta_min=eta_min)
    # training
    os.makedirs('../sotmax_pretrain_checkpoints', exist_ok=True)
    best_loss = 10000.
    # mb = master_bar(range(N_EPOCHS))
    for e in range(RESUME_EPOCH, N_EPOCHS + 1):
        classifier.train()
        avg_loss = 0.
        start_time = time.time()

        for batch_mel_db, batch_y in tqdm(train_loader):
            # print('batch_mel_db shape: {}'.format(batch_mel_db.shape))
            batch_mel_db = torch.reshape(batch_mel_db, (BATCH_SIZE * NUM_UTTER_PER_SPEAKER,
                                                                batch_mel_db.size(2),
                                                                batch_mel_db.size(3)))

            preds = classifier(batch_mel_db.cuda())
            # print('preds shape: {}'.format(preds.shape))
            # print('batch_y shape: {}'.format(batch_y.shape))
            # loss = criterion(preds, batch_y.cuda())
            loss = criterion(preds, torch.max(batch_y.cuda(), 1)[1])

            # update model weights
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            # print('gradient updated!')
            avg_loss += loss.item() / len(train_loader)


        classifier.eval()
        avg_val_loss = 0.
        correct_count = 0
        count = 0
        for val_batch_id, (val_batch_mel_db, val_batch_y) in enumerate(valid_loader):
            val_batch_mel_db = torch.reshape(val_batch_mel_db, (BATCH_SIZE * NUM_UTTER_PER_SPEAKER,
                                                        val_batch_mel_db.size(2),
                                                        val_batch_mel_db.size(3)))
            val_preds = classifier(val_batch_mel_db.cuda())

            # compute validation loss
            val_loss = criterion(val_preds, torch.max(val_batch_y.cuda(), 1)[1])
            avg_val_loss += val_loss.item() / len(valid_loader)

            # accuracy
            # correct_count += (val_preds.data == val_batch_y.cuda()).double().sum().data[0]
            # correct_count += (np.argmax(val_preds.cpu().detach().numpy(), 1) == np.argmax(val_batch_y.cpu().numpy(), 1)).sum().data[0]
            val_preds = torch.max(val_preds.data, 1)[1]
            correct_count += val_preds.eq(torch.max(val_batch_y.cuda().data, 1)[1]).float().cpu().sum()
            count += val_batch_y.shape[0]

        val_acc = correct_count / count
        elapsed = time.time() - start_time
        megs = f'Epoch {e + 1} - avg_train_loss: {avg_loss:.4f}  avg_val_loss: {avg_val_loss:.4f} val_acc: {val_acc:.4f} time: {elapsed:.0f}s' # .format(e + 1, avg_loss,
                                                                            # avg_val_loss, elapsed)
        print(megs)

        # scheduler.step()

        if avg_val_loss < best_loss:
            best_loss = avg_val_loss
            torch.save(classifier.state_dict(), os.path.join('../sotmax_pretrain_checkpoints',
                                                             f'cls_{e+1}_{avg_val_loss:.4f}'))

if __name__ == '__main__':

    # preprocessing
    # audio_preprocess('../meta/vin_train.txt', 'train')
    # audio_preprocess('../meta/vin_val.txt', 'valid')

    train()






















