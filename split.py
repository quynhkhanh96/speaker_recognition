import os
import shutil
import numpy as np
import pandas as pd

DATA_PATH = '/home/khanhdtq/Downloads/vietnamese_wav'
val_dir = '/home/khanhdtq/Downloads/valid_wav'
test_dir = '/home/khanhdtq/Downloads/test_wav'

# list_speakers = os.listdir(DATA_PATH)
# num_speakers = len(list_speakers)
# print('Total number of speakers = {}'.format(num_speakers))
#
# train_speakers = list_speakers[: int(0.8 * num_speakers)]
# val_speakers = list_speakers[int(0.8 * num_speakers): int(0.9 * num_speakers)]
# test_speakers = list_speakers[int(0.9 * num_speakers): ]
#
# print('Number train speakers = {} - valid speakers = {} - test speakers = {}'.format(
#     len(train_speakers), len(val_speakers), len(test_speakers)
# ))
#
# os.makedirs(val_dir, exist_ok=True)
# os.makedirs(test_dir, exist_ok=True)
#
# print('Creating valid data ...')
# for spkr in val_speakers:
#     new_dir = os.path.join(val_dir, spkr)
#     os.makedirs(new_dir, exist_ok=True)
#     spkr_dir = os.path.join(DATA_PATH, spkr)
#     for spkr_file in os.listdir(spkr_dir):
#         shutil.move(os.path.join(spkr_dir, spkr_file), os.path.join(new_dir, spkr_file))
#
# print('Creating test data ...')
# for spkr in test_speakers:
#     new_dir = os.path.join(test_dir, spkr)
#     os.makedirs(new_dir, exist_ok=True)
#     spkr_dir = os.path.join(DATA_PATH, spkr)
#     for spkr_file in os.listdir(spkr_dir):
#         shutil.move(os.path.join(spkr_dir, spkr_file), os.path.join(new_dir, spkr_file))

empty_dirs = [dir_ for dir_ in os.listdir(DATA_PATH) if len(os.listdir(os.path.join(DATA_PATH, dir_))) < 10]
for dir_ in empty_dirs:
    # os.rmdir(os.path.join(DATA_PATH, dir_))
    shutil.rmtree(os.path.join(DATA_PATH, dir_))

# for dataset in [DATA_PATH, val_dir, test_dir]:
#     num_file_each_speaker = []
#     for spkr in os.listdir(dataset):
#         num_file_each_speaker.append(len(os.listdir(os.path.join(dataset, spkr))))
#
#     print(sorted(list(set(num_file_each_speaker))))
#     min_nfile = sorted(num_file_each_speaker)[0]
#     max_nfile = sorted(num_file_each_speaker)[-1]
#
#     print(dataset.split('/')[-1])
#     print('min_nfile = {} - max_nfile = {}'.format(min_nfile, max_nfile))